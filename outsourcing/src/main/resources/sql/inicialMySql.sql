use outsourcing;
create table Vertical(
CodigoVertical int PRIMARY KEY,
NombreVertical char(40)
);
create table Rol(
CodigoRol int primary Key,
NombreRol char(20)
);
create table Cargo(
CodigoCargo int primary Key,
NombreCargo char(20)
);
create table Cliente(
NombreCliente char(20),
Nit int PRIMARY KEY,
FechaContacto Date,
CodDirector int,
CodigoVertical int,
CodigoUsuarioDirector int,
foreign key (CodigoVertical) references Vertical(CodigoVertical)
);


create table Proyecto(
CodigoProyecto int PRIMARY KEY,
NombreProyecto char(40),
NitCliente int,
foreign key (NitCliente) references Cliente (Nit)
);
create table Requerimiento(
CodigoRequerimiento int PRIMARY KEY,
NombreRequerimiento char(40),
CodigoProyecto int,
foreign key (CodigoProyecto) references Proyecto (CodigoProyecto)
);
create table Usuario
(
CodigoUsuario bigint primary key,
Apellidos Varchar (40),
Nombres Varchar (40),
Edad int,
CodigoVertical int,
Direccion Varchar (50),
FechaContrato date,
NombrePC Varchar (50),
ParentescoPC Varchar (30),
TelefonoPC int,
FechaNacimiento date,
CodigoRol int,
CodigoCargo int,
FechaIngreso Date,
foreign key (CodigoVertical) references Vertical (CodigoVertical),
foreign key (CodigoRol) references Rol (CodigoRol),
foreign key (CodigoCargo) references Cargo (CodigoCargo)
);
create table Captura
(
CodigoCaptura int Primary Key,
CodigoRequerimiento int,
Observaciones varchar (140),
CodigoUsuario bigint,
FechaActividad date,
Duracion int,
FechaRegActividad date,
foreign key (CodigoRequerimiento) references Requerimiento (CodigoRequerimiento),
foreign key (CodigoUsuario) references Usuario (CodigoUsuario)
);
