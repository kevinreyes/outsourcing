package Proyectos;
import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.ProyectosBoundary;
import entities.Proyecto;

@ManagedBean
public class ProyectoController {
	private List<Proyecto> proyectos;

	public List<Proyecto> getProyectos() {
		ProyectosBoundary proyectosBoundary = new ProyectosBoundary();
		proyectos = proyectosBoundary.obtenerProyectos();
		return proyectos;
	}

	public void setProyectos(List<Proyecto> proyectos) {
		this.proyectos = proyectos;
	}	

}
