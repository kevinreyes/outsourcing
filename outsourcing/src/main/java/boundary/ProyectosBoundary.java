package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Proyecto;

public class ProyectosBoundary {
	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public ProyectosBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Proyecto> obtenerProyectos() {
		List<Proyecto> proyectos = (List<Proyecto>) entityManager.createQuery("FROM Proyecto").getResultList();
		return proyectos;
	}

	
}
