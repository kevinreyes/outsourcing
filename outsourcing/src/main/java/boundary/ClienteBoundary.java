package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Cliente;

public class ClienteBoundary {
	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public ClienteBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}
    @SuppressWarnings("unchecked")
	public List<Cliente> obtenerClientes() {
		List<Cliente> clientes = (List<Cliente>) entityManager.createQuery("FROM Cliente").getResultList();
		return clientes;
	}
}
