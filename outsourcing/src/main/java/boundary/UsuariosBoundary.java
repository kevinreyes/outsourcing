package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Usuario;

public class UsuariosBoundary {

	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public UsuariosBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}

	// Metodo que consulta todos los usuarios
	@SuppressWarnings("unchecked")
	public List<Usuario> obtenerUsuarios() {
		List<Usuario> usuarios = (List<Usuario>) entityManager.createQuery("FROM Usuario").getResultList();
		return usuarios;
	}
}
