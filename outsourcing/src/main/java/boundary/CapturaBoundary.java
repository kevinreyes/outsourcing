package boundary;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Captura;
public class CapturaBoundary {
	
	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	public CapturaBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}
	@SuppressWarnings("unchecked")
	public List<Captura> listarCapturas(){
		List<Captura> capturas = (List<Captura>) entityManager.createQuery("from captura").getResultList();
		return capturas;
		
	}
	
}
