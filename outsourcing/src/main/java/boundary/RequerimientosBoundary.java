package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Requerimiento;

public class RequerimientosBoundary {
	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public RequerimientosBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Requerimiento> obtenerRequerimientos() {
		List<Requerimiento> requerimientos = (List<Requerimiento>) entityManager.createQuery("FROM Requerimiento").getResultList();
		return requerimientos;
	}

	
}
