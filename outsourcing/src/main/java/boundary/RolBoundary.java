package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Rol;

public class RolBoundary {
	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public RolBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}
    
	@SuppressWarnings("unchecked")
	public List<Rol> obtenerRoles() {
		List<Rol> roles = (List<Rol>) entityManager.createQuery("FROM Rol").getResultList();
		return roles;
	}
}
