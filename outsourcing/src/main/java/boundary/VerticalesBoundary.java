package boundary;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;

import entities.Vertical;

public class VerticalesBoundary {

	private static EntityManager entityManager;
	private static EntityManagerFactory entityManagerFactory;
	@Inject
	Logger logger;

	public VerticalesBoundary() {
		entityManagerFactory = Persistence.createEntityManagerFactory("outsourcing");
		entityManager = entityManagerFactory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Vertical> obtenerVerticales() {
		List<Vertical> verticales = (List<Vertical>) entityManager.createQuery("FROM Vertical").getResultList();
		return verticales;
	}
}
