package Verticales;

import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.UsuariosBoundary;
import boundary.VerticalesBoundary;
import entities.Vertical;

@ManagedBean
public class VerticalesController {

	private List<Vertical> verticales;

	public List<Vertical> getVerticales() {
		VerticalesBoundary verticalesBoundary = new VerticalesBoundary();
		verticales = verticalesBoundary.obtenerVerticales();
		return verticales;
	}

	public void setVerticales(List<Vertical> verticales) {
		this.verticales = verticales;
	}	
	
}
