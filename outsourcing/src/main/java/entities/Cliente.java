/**
 * 
 */
package entities;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@ViewScoped
@ManagedBean
@Entity
@Table(name = "Cliente")
public class Cliente {
	@Id
	@Column
	private int Nit;
	@Column
	private String NombreCliente;
	@Column
	private Date FechaContacto;
	@Column
	private int CodDirector;
	@Column
	private int CodigoVertical;
	@Column
	private int CodigoUsuarioDirector;
	
	public int getCodigoUsuarioDirector() {
		return CodigoUsuarioDirector;
	}
	public void setCodigoUsuarioDirector(int codigoUsuarioDirector) {
		CodigoUsuarioDirector = codigoUsuarioDirector;
	}
	public int getNit() {
		return Nit;
	}
	public void setNit(int nit) {
		Nit = nit;
	}
	public String getNombreCliente() {
		return NombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		NombreCliente = nombreCliente;
	}
	public Date getFechaContacto() {
		return FechaContacto;
	}
	public void setFechaContacto(Date fechaContacto) {
		FechaContacto = fechaContacto;
	}
	public int getCodDirector() {
		return CodDirector;
	}
	public void setCodDirector(int codDirector) {
		CodDirector = codDirector;
	}
	public int getCodigoVertical() {
		return CodigoVertical;
	}
	public void setCodigoVertical(int codVertical) {
		CodigoVertical = codVertical;
	}
	
}
