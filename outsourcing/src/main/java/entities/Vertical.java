package entities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@ViewScoped
@ManagedBean
@Entity
@Table (name = "Vertical")
public class Vertical {
	@Id
	@Column
	private int CodigoVertical;
	@Column
	private String NombreVertical;

	public int getCodigoVertical() {
		return CodigoVertical;
	}

	public void setCodigoVertical(int codigoVertical) {
		CodigoVertical = codigoVertical;
	}

	public String getNombreVertical() {
		return NombreVertical;
	}

	public void setNombreVertical(String nombreVertical) {
		NombreVertical = nombreVertical;
	}

}
