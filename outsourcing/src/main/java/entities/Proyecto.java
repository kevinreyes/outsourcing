package entities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@ViewScoped
@ManagedBean
@Entity
@Table(name = "Proyecto")
public class Proyecto {
	@Id
	private int CodigoProyecto;
	private String NombreProyecto;
	private int NitCliente;

	public int getCodigoProyecto() {
		return CodigoProyecto;
	}

	public void setCodigoProyecto(int codigoProyecto) {
		CodigoProyecto = codigoProyecto;
	}

	public String getNombreProyecto() {
		return NombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		NombreProyecto = nombreProyecto;
	}

	public int getNitCliente() {
		return NitCliente;
	}

	public void setNitCliente(int NitCliente) {
		this.NitCliente = NitCliente;
	}

}
