package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Rol {
	@Id
	@Column
	private int CodigoRol;
	@Column
	private String NombreRol;
	public int getCodigoRol() {
		return CodigoRol;
	}
	public void setCodigoRol(int codigoRol) {
		CodigoRol = codigoRol;
	}
	public String getNombreRol() {
		return NombreRol;
	}
	public void setNombreRol(String nombreRol) {
		NombreRol = nombreRol;
	}
	
}
