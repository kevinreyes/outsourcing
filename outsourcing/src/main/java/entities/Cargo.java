package entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cargo {
	@Id
	private int Codigo;
	private String NombreCargo;
	public int getCodigo() {
		return Codigo;
	}
	public void setCodigo(int codigo) {
		Codigo = codigo;
	}
	public String getNombreCargo() {
		return NombreCargo;
	}
	public void setNombreCargo(String nombreCargo) {
		NombreCargo = nombreCargo;
	}
	
}
