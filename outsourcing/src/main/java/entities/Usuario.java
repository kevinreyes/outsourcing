package entities;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@ViewScoped
@ManagedBean
@Entity
@Table (name = "Usuario")
public class Usuario {
	@Id
	@Column
	private Long CodigoUsuario;
	@Column
	private String Apellidos;
	@Column
	private String Nombres;
	@Column
	private Long Edad;
	@Column
	private Long CodigoVertical;
	@Column
	private String Direccion;
	@Column
	private Date FechaContrato;
	@Column
	private String NombrePC;
	@Column
	private String ParentescoPC;
	@Column
	private Long TelefonoPC;
	@Column
	private Date FechaNacimiento;
	@Column
	private Long CodigoRol;
	@Column
	private Long CodigoCargo;
	@Column
	private Date FechaIngreso;
	
	public Usuario() {
	}
	public Long getCodigoUsuario() {
		return CodigoUsuario;
	}
	public void setCodigoUsuario(Long codigoUsuario) {
		CodigoUsuario = codigoUsuario;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public Long getEdad() {
		return Edad;
	}
	public void setEdad(Long edad) {
		Edad = edad;
	}
	public Long getCodigoVertical() {
		return CodigoVertical;
	}
	public void setCodigoVertical(Long codigoVertical) {
		CodigoVertical = codigoVertical;
	}
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	public Date getFechaContrato() {
		return FechaContrato;
	}
	public void setFechaContrato(Date fechaContrato) {
		FechaContrato = fechaContrato;
	}
	public String getNombrePC() {
		return NombrePC;
	}
	public void setNombrePC(String nombrePC) {
		NombrePC = nombrePC;
	}
	public String getParentescoPC() {
		return ParentescoPC;
	}
	public void setParentescoPC(String parentescoPC) {
		ParentescoPC = parentescoPC;
	}
	public Long getTelefonoPC() {
		return TelefonoPC;
	}
	public void setTelefonoPC(Long telefonoPC) {
		TelefonoPC = telefonoPC;
	}
	public Date getFechaNacimiento() {
		return FechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}
	public Long getCodigoRol() {
		return CodigoRol;
	}
	public void setCodigoRol(Long codigoRol) {
		CodigoRol = codigoRol;
	}
	public Long getCodigoCargo() {
		return CodigoCargo;
	}
	public void setCodigoCargo(Long codigoCargo) {
		CodigoCargo = codigoCargo;
	}
	public Date getFechaIngreso() {
		return FechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		FechaIngreso = fechaIngreso;
	}
	@Override
	public String toString() {
		return "Usuario [CodigoUsuario=" + CodigoUsuario + ", Apellidos=" + Apellidos + ", Nombres=" + Nombres
				+ ", Edad=" + Edad + ", CodigoVertical=" + CodigoVertical + ", Direccion=" + Direccion
				+ ", FechaContrato=" + FechaContrato + ", NombrePC=" + NombrePC + ", ParentescoPC=" + ParentescoPC
				+ ", TelefonoPC=" + TelefonoPC + ", FechaNacimiento=" + FechaNacimiento + ", CodigoRol=" + CodigoRol
				+ ", CodigoCargo=" + CodigoCargo + ", FechaIngreso=" + FechaIngreso + "]";
	}
	
}
