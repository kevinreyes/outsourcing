package entities;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@ViewScoped
@ManagedBean
@Entity
@Table (name = "Captura")
public class Captura {
	@Id
	private int CodigoCaptura;
	private int CodigoRequerimiento;
	private String Observaciones;
	private int CodigoUsuario;
	private Date FechaActividad;
	private int Duracion;
	private Date FechaRegActividad;
	public int getCodigoCaptura() {
		return CodigoCaptura;
	}
	public void setCodigoCaptura(int codigoCaptura) {
		CodigoCaptura = codigoCaptura;
	}
	public int getCodigoRequerimiento() {
		return CodigoRequerimiento;
	}
	public void setCodigoRequerimiento(int codigoRequerimiento) {
		CodigoRequerimiento = codigoRequerimiento;
	}
	public String getObservaciones() {
		return Observaciones;
	}
	public void setObservaciones(String observaciones) {
		Observaciones = observaciones;
	}
	public int getCodigoUsuario() {
		return CodigoUsuario;
	}
	public void setCodigoUsuario(int codigoUsuario) {
		CodigoUsuario = codigoUsuario;
	}
	public Date getFechaActividad() {
		return FechaActividad;
	}
	public void setFechaActividad(Date fechaActividad) {
		FechaActividad = fechaActividad;
	}
	public int getDuracion() {
		return Duracion;
	}
	public void setDuracion(int duracion) {
		Duracion = duracion;
	}
	public Date getFechaRegActividad() {
		return FechaRegActividad;
	}
	public void setFechaRegActividad(Date fechaRegActividad) {
		FechaRegActividad = fechaRegActividad;
	}
	
	
}
