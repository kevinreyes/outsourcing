package entities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@ViewScoped
@ManagedBean
@Entity
@Table(name = "Requerimiento")
public class Requerimiento {
	@Id
	private int CodigoRequerimiento;
	private String NombreRequerimiento;
	private int CodigoProyecto;
	public int getCodigoProyecto() {
		return CodigoProyecto;
	}
	public void setCodigoProyecto(int codigoProyecto) {
		CodigoProyecto = codigoProyecto;
	}
	public int getCodigoRequerimiento() {
		return CodigoRequerimiento;
	}
	public void setCodigoRequerimiento(int codigoRequerimiento) {
		CodigoRequerimiento = codigoRequerimiento;
	}
	public String getNombreRequerimiento() {
		return NombreRequerimiento;
	}
	public void setNombreRequerimiento(String nombreRequerimiento) {
		NombreRequerimiento = nombreRequerimiento;
	}
	
}
