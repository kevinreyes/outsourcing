package Rol;

import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.RolBoundary;
import entities.Rol;

@ManagedBean
public class RolController {
	private List<Rol> roles;

	public List<Rol> getRoles() {
		RolBoundary rolBoundary = new RolBoundary();
		roles = rolBoundary.obtenerRoles();
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
}
