package Clientes;
import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.ClienteBoundary;
import boundary.RequerimientosBoundary;
import entities.Cliente;

@ManagedBean
public class ClienteController {
	private List<Cliente> clientes;

	public List<Cliente> getClientes() {
		ClienteBoundary clienteBoundary = new ClienteBoundary();
		clientes = clienteBoundary.obtenerClientes();
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	

	

}
