package Usuarios;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import boundary.UsuariosBoundary;
import entities.Usuario;

@ViewScoped
@ManagedBean
public class UsuarioController {

	private List<Usuario> usuarios;
	@Inject
	Logger logger;

	public List<Usuario> getUsuarios() {
		UsuariosBoundary usuariosBoundary = new UsuariosBoundary();
		usuarios = usuariosBoundary.obtenerUsuarios();
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	

}
