package Requerimientos;


import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.RequerimientosBoundary;
import entities.Requerimiento;

@ManagedBean
public class RequerimientosController {
	private List<Requerimiento> requerimientos;

	public List<Requerimiento> getRequerimientos() {
		RequerimientosBoundary requerimientosBoundary = new RequerimientosBoundary();
		requerimientos = requerimientosBoundary.obtenerRequerimientos();
		return requerimientos;
	}

	public void setRequerimientos(List<Requerimiento> requerimientos) {
		this.requerimientos = requerimientos;
	}	



}
