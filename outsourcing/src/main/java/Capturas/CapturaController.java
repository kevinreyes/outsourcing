package Capturas;

import java.util.List;

import javax.faces.bean.ManagedBean;

import boundary.CapturaBoundary;
import entities.Captura;

@ManagedBean
public class CapturaController {
	private List<Captura> capturas;
	public String nombreCliente;
	public List<Captura> getCapturas() {
		CapturaBoundary capturaBoundary = new CapturaBoundary();
		capturas = capturaBoundary.listarCapturas();
		return capturas;
	}
	public void setCapturas(List<Captura> capturas) {
		this.capturas = capturas;
	}
	public String getNombreCliente() {
		
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	
	
}
